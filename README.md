# Cabify Code Challenge

This repository contains two subrepositories with the test performed in both PHP and Ruby.

The instructions to check the code can be found in the `Readme.md` file of each of the repositories.

Project can be found on Gitlab:
 * https://gitlab.com/fjalvarez/code_challenge_php
 * https://gitlab.com/fjalvarez/code_challenge_ruby

Thanks

Francisco Jose Alvarez de Diego
fjalvarez@gmail.com
+34 657 10 79 63